﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsHolder : MonoBehaviour
{
    public static SettingsHolder instance;

    public int maxNumberOfBalls = 10;
    public int maxMass = 5;
    public float ballStartingSize = 0.1f;
    public float pullRadious = 10f;
    public float pullForceMultiplier = 0.1f;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (this != instance)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
}
