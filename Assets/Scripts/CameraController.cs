﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] public float mouseSensitivity = 5f;
    [SerializeField] float moveSpeed = 12f;

    private bool active = true;

    private float xMovement;
    private float zMovement;

    private float xRotation;
    private float yRotation;

    private float mouseX;
    private float mouseY;

    private Vector3 flyMode;
    private Vector3 moveMode;
    private Vector3 moveVector;


    void Update()
    {
        if (active)
        {
            xMovement = Input.GetAxis("Horizontal");
            zMovement = Input.GetAxis("Vertical");

            flyMode = transform.right * xMovement + transform.forward * zMovement;
            moveMode = new Vector3(flyMode.x, 0f, flyMode.z);

            moveVector = moveMode * moveSpeed * Time.deltaTime;

            transform.position += moveVector;


            mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
            mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

            xRotation -= mouseY;
            yRotation += mouseX;
            transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0f);
        }
    }
    public bool Active
    {
        get
        {
            return active;
        }
        set
        {
            active = value;
        }
    }

}
