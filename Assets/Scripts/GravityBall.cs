﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GravityBall : MonoBehaviour
{
    [SerializeField] private int maxMass = 50;
    [SerializeField] private float ballStartingSize = 0.1f;
    [SerializeField] private float pullForceMultiplier = 0.1f;
    public int BallNumber { get; set; }

    //Defines if balls are pulling towards eachOther or pushing away
    public static int pullOrPushFactor = 1;
    public static bool joiningEnabled = true;

    public static List<GravityBall> activeBalls = new List<GravityBall>();

    private float radious;
    public bool pullingEnabled = true;
    private Rigidbody rBody;
    private SphereCollider collider;

    //Mass in other words means number of single balls creating this ball
    public int Mass { get; set; }

    private void Awake()
    {
        rBody = GetComponent<Rigidbody>();
        collider = GetComponent<SphereCollider>();
        Mass = 1;
        Radious = GetNewRadious();
    }

    void Update()
    {
        if (Mass >= maxMass)
        {
            Explode();
        }
    }

    private void FixedUpdate()
    {
        if (pullingEnabled)
        {
            PullOrPushOtherBalls();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(!joiningEnabled)
        {
            return;
        }
        GravityBall otherBall = collision.gameObject.GetComponent<GravityBall>();
        if (otherBall)
        {
            if (Mass > otherBall.Mass)
            {
                DevourBall(otherBall);
            }
            else if (Mass == otherBall.Mass)
            {
                if (BallNumber > otherBall.BallNumber)
                {
                    DevourBall(otherBall);
                }
                else
                {
                    otherBall.DevourBall(this);
                }
            }
            else
            {
                otherBall.DevourBall(this);
            }
        }
    }

    public void Explode()
    {
        BallSpawner.instance.BallExplosion(Mass, transform.position);
        activeBalls.Remove(this);
        Destroy(gameObject);
    }

    public void DevourBall(GravityBall otherBall)
    {
        Mass += otherBall.Mass;
        rBody.mass = Mass;
        Radious = GetNewRadious();
        BallSpawner.instance.AddNewBallToQueue();
        activeBalls.Remove(otherBall);
        Destroy(otherBall.gameObject);
    }

    public abstract void PullOrPushOtherBalls();

    public void PullOrPushBall(Vector3 pullVector, float massAndDistanceCoefficient)
    {
        if (!pullingEnabled)
        {
            return;
        }
        rBody.AddForce(pullVector * massAndDistanceCoefficient * pullForceMultiplier * pullOrPushFactor);
    }

    public void LaunchBall(Vector3 velocity)
    {
        rBody.velocity = velocity;
    }

    public void TurnOnCollisionAndPullingAfterTime(float time)
    {
        StartCoroutine(TurnOnColisionAndPullingCoroutine(time));
    }

    public void TurnOffColliderAndPulling()
    {
        pullingEnabled = false;
        collider.enabled = false;
    }

    public Vector3 GetNormalizedPullVector(GravityBall otherBall)
    {
        Vector3 pullVector = transform.position - otherBall.transform.position;
        return pullVector.normalized;
    }

    public float GetMassAndDistanceCoefficient(GravityBall otherBall)
    {
        Vector3 pullVector = transform.position - otherBall.transform.position;
        return (Mass * otherBall.Mass / Mathf.Pow(pullVector.magnitude, 2));
    }


    public float Radious
    {
        get
        {
            return radious;
        }
        set
        {
            radious = value;
            transform.localScale = new Vector3(value, value, value);
        }
    }

    public float BallRadiousMultiplier
    {
        get
        {
            return ballStartingSize;
        }

        set
        {
            ballStartingSize = value;
        }
    }
    public int MaxMass
    {
        get
        {
            return maxMass;
        }
        set
        {
            maxMass = value;
        }
    }
    public float PullForceMultiplier
    {
        get
        {
            return pullForceMultiplier;
        }
        set
        {
            pullForceMultiplier = value;
        }

    }

    private IEnumerator TurnOnColisionAndPullingCoroutine(float time)
    {
        yield return new WaitForSeconds(time);
        collider.enabled = true;
        pullingEnabled = true;
    }

    private float GetNewRadious()
    {
        return Mathf.Abs(Mathf.Pow(3f / 4f / Mathf.PI * Mass, 1f / 3f)) * ballStartingSize;
    }
}
