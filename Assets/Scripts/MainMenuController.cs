﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private InputField maxNumberOfBalls;
    [SerializeField] private InputField maxMass;
    [SerializeField] private InputField ballStartingSize;
    [SerializeField] private InputField pullRadious;
    [SerializeField] private InputField pullForceMultiplier;

    private void Start()
    {
        SetSettingsInputFields();
    }

    public void StartScene(int sceneNumber) => SceneManager.LoadScene(sceneNumber);

    public void QuitGame() => Application.Quit();

    public void SetSettingsInputFields()
    {
        maxNumberOfBalls.text = SettingsHolder.instance.maxNumberOfBalls.ToString();
        maxMass.text = SettingsHolder.instance.maxMass.ToString();
        ballStartingSize.text = SettingsHolder.instance.ballStartingSize.ToString();
        pullRadious.text = SettingsHolder.instance.pullRadious.ToString();
        pullForceMultiplier.text = SettingsHolder.instance.pullForceMultiplier.ToString();
    }

    public void ModifySettings()
    {
        int input = VerifyIntInput(maxNumberOfBalls.text);
        if(input != 0)
        {
            SettingsHolder.instance.maxNumberOfBalls = input;
        }

        input = VerifyIntInput(maxMass.text);
        if (input != 0)
        {
            SettingsHolder.instance.maxMass = input;
        }

        float fInput = VerifyFloatInput(ballStartingSize.text);
        if (fInput != 0)
        {
            SettingsHolder.instance.ballStartingSize = fInput;
        }

        fInput = VerifyFloatInput(pullRadious.text);
        if (fInput != 0)
        {
            SettingsHolder.instance.pullRadious = fInput;
        }

        fInput = VerifyFloatInput(pullForceMultiplier.text);
        if (fInput != 0)
        {
            SettingsHolder.instance.pullForceMultiplier = fInput;
        }
    }

    //All int values we verify must be greater than zero
    private int VerifyIntInput(string input)
    {
        int number;
        bool success = Int32.TryParse(input, out number);
        if(success)
        {
            if(number < 0)
            {
                return 0;
            }
            return number;
        }

        return 0;
    }

    private float VerifyFloatInput(string input)
    {
        float number;
        bool success = Single.TryParse(input, out number);
        if (success)
        {
            if (number < 0)
            {
                return 0f;
            }
            return number;
        }

        return 0f;
    }

}
