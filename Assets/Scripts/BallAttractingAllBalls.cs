﻿using UnityEngine;
using System.Collections;


public class BallAttractingAllBalls : GravityBall
{
    public override void PullOrPushOtherBalls()
    {
        foreach (GravityBall ball in activeBalls)
        {
            if (ball != this)
            {
                Vector3 pullVector = base.GetNormalizedPullVector(ball);
                float pullCoefficient = base.GetMassAndDistanceCoefficient(ball);
                ball.PullOrPushBall(pullVector, pullCoefficient);
            }
        }
    }
}