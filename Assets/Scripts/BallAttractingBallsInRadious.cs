﻿using UnityEngine;
using System.Collections;


public class BallAttractingBallsInRadious : GravityBall
{
    //Distance at which forces work
    [SerializeField] private float pullRadious = 10f;
    public override void PullOrPushOtherBalls()
    {
        LayerMask mask = LayerMask.GetMask("Ball");
        Collider[] balls = Physics.OverlapSphere(transform.position, pullRadious, mask);
        foreach (Collider collider in balls)
        {
            GravityBall ball = collider.gameObject.GetComponent<GravityBall>();
            if (ball != this)
            {
                Vector3 pullVector = base.GetNormalizedPullVector(ball);
                float pullCoefficient = base.GetMassAndDistanceCoefficient(ball);
                ball.PullOrPushBall(pullVector, pullCoefficient);
            }
        }
    }

    public float PullRadious
    {
        get
        {
            return pullRadious;
        }

        set
        {
            pullRadious = value;
        }
    }
}
