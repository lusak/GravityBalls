﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    public static BallSpawner instance;
    public const string BALLS_PUSHING_AWAY_TEXT = "Balls pushing away from eachother";

    [SerializeField] private GravityBall ballPrefab;
    [SerializeField] private int maxNumberOfBalls = 250;

    //Spawning area is a cube with side length as below
    [SerializeField] private float spawningAreaSize = 5f;
    [SerializeField] private float timeBetweeneSpawns = 0.25f;
    [SerializeField] private float maxExplosionSpeed = 10f;
    [SerializeField] private float ballNotUnderForcesTime = 0.5f;

    [SerializeField] private TextMeshProUGUI ballsCreatedNumberText;
    [SerializeField] private TextMeshProUGUI ballsPullingOrPushingText;

    private int ballIndex = 0;
    private int ballsCreated = 0;
    private int startingBallPoolSize;
    private Queue<GravityBall> ballsPool;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (this != instance)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        GravityBall.pullOrPushFactor = 1;
        GravityBall.joiningEnabled = true;
        SetupPrefab();
        startingBallPoolSize = maxNumberOfBalls + 100;
        CreateBallsPool();
        StartCoroutine(SpawnBalls());
    }

    public void BallExplosion(int numberOfBalls, Vector3 position)
    {
        for (int i = 0; i < numberOfBalls; i++)
        {
            GravityBall ball = ballsPool.Dequeue();
            GravityBall.activeBalls.Add(ball);
            ballIndex++;
            ball.BallNumber = ballIndex;
            ball.TurnOffColliderAndPulling();
            ball.gameObject.transform.position = position;
            ball.gameObject.SetActive(true);
            ball.LaunchBall(GenerateExplosionVector());
            ball.TurnOnCollisionAndPullingAfterTime(ballNotUnderForcesTime);
        }
    }

    public void AddNewBallToQueue()
    {
        if (ballsPool.Count <= startingBallPoolSize)
        {
            GravityBall ball = Instantiate(ballPrefab);
            ball.gameObject.SetActive(false);
            ballsPool.Enqueue(ball);
        } 
        return;
    }

    public int MaxNumberOfBalls { get; set; }

    private void CreateBallsPool()
    {
        ballsPool = new Queue<GravityBall>();
        for (int i = 0; i < startingBallPoolSize; i++)
        {
            GravityBall ball = Instantiate(ballPrefab);
            ball.gameObject.SetActive(false);
            ballsPool.Enqueue(ball);
        }
    }

    private IEnumerator SpawnBalls()
    {
        while (ballsCreated < maxNumberOfBalls)
        {
            GravityBall ball = ballsPool.Dequeue();
            GravityBall.activeBalls.Add(ball);
            ball.gameObject.transform.position = GenerateSpawnPosition();
            ball.gameObject.SetActive(true);
            ballsCreated++;
            ballIndex++;
            ball.BallNumber = ballIndex;
            ballsCreatedNumberText.text = ballsCreated.ToString();
            yield return new WaitForSeconds(timeBetweeneSpawns);
        }
        Debug.Log("Started pushing away");
        ballsPullingOrPushingText.text = BALLS_PUSHING_AWAY_TEXT;
        GravityBall.pullOrPushFactor = -1;
        GravityBall.joiningEnabled = false;
    }

    private Vector3 GenerateSpawnPosition()
    {
        float x = Random.Range(-spawningAreaSize, spawningAreaSize);
        float y = Random.Range(-spawningAreaSize, spawningAreaSize);
        float z = Random.Range(-spawningAreaSize, spawningAreaSize);
        return new Vector3(x, y, z);
    }

    private Vector3 GenerateExplosionVector()
    {
        float x = GenerateExplosionVelocity();
        float y = GenerateExplosionVelocity();
        float z = GenerateExplosionVelocity();
        return new Vector3(x, y, z);
    }

    private float GenerateExplosionVelocity()
    {
        float velocity = Random.Range(0, maxExplosionSpeed);

        if(Random.Range(0, 2) == 0)
        {
            return velocity;
        }

        return -velocity;
    }

    private void SetupPrefab()
    {
        ballPrefab.MaxMass = SettingsHolder.instance.maxMass;
        ballPrefab.PullForceMultiplier = SettingsHolder.instance.pullForceMultiplier;
        ballPrefab.BallRadiousMultiplier = SettingsHolder.instance.ballStartingSize;
        maxNumberOfBalls = SettingsHolder.instance.maxNumberOfBalls;

        if(ballPrefab is BallAttractingBallsInRadious)
        {
            BallAttractingBallsInRadious ball = (BallAttractingBallsInRadious)ballPrefab;
            ball.PullRadious = SettingsHolder.instance.pullRadious;
        }
    }
}
