Gravity balls is a 3D simulation.
Every short period of time new ball is created at random place in the scene.
Ball attracts other balls. When two balls collide, they join to create one bigger ball.
When ball reaches maximum size it explodes into as many small balls as it was created from.
After some time instead of attracting each other, balls start to push each other away.
Gravity is switched off in the scene, but Drag is working.
Object Pooling was used to help with the performance.

Three different models:
- ball attracting every other ball
- ball attracting balls that are within certain radious
- ball attracted by center of mass of the scene